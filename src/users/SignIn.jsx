import { useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import { signIn } from '../store/user';

/** Redux
 *
 * <<useDispatch>> Hook que permite acceder a los reducers realizados en el createSlice del store
 *  este hook tiene una funcion en la cual se puede llamar a la funcion necesaria para modificar los
 *  valores del store.
 *
 */

/** react hook form
 *
 *  <<useForm>> Hook que permite manipular de una mejor manera los datos recogidos de un form.
 * 		devuelve 2 elementos.
 *
 * 		<register> Este permite avisarle al hook cuales seran los datos que tiene que tomar.
 * 		dentro del input se clona el elemento con {...} y se coloca el register con el nombre
 * 		que se desea que posea en el obj. De esta manera {...register('email')}
 *
 * 		<handleSubmit> Esta funcion recibe una funcion en la cual se indica que se hara con
 * 		los datos del formulario. Entrega un obj con la informacion tomada del form.
 */

const SignIn = () => {
	const dispatch = useDispatch();
	const { register, handleSubmit } = useForm();

	const onSubmit = (data) => {
		dispatch(
			signIn({
				credentials: data,
			})
		);
	};

	return (
		<form onSubmit={handleSubmit(onSubmit)}>
			<input type='email' name='email' {...register('email')} />
			<input type='password' name='password' {...register('password')} />
			<input type='submit' value='Ingresar' />
		</form>
	);
};

export default SignIn;
