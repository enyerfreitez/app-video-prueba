import { configureStore, combineReducers } from '@reduxjs/toolkit';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import userSlice from './user';

/** Redux
 *
 * <<IMPORTANTE>> Los Storage creados son almacenados en memoria. Es necesario guardarlos en otros lados para se sean persistentes)
 *
 * <<Store>> Se podria decir que es un contenedor el cual posee en su interior la informacion necesaria
 *  para la app y las funciones para modificar las mismas, ya que es la unica forma de que dicha info sea
 *  modificada, ya que desde afuera solo se puede consultar la data.
 *
 * <<configureStore>> Esta funcion es la que configura el store, esta recibe un obj donde uno de sus
 *  elementos es <<reducer>> Estos son los diferentes stores que usaremos en nuestras app. Pueden haber
 *  tantos en nuestras app segun modulos tengamos o necesitemos.
 *
 * <<combineReducers>> Es una funcion la cual recibe los reducer creados como parametros y permite juntarlos para manejarlos juntos
 *  y fuera del <<configureStore>>. Esto es necesario para poder usar la funcionalidad de la libreria redux-persist
 * 
 */

/** Redux-persist
 * 
 * 	<<IMPORTANTE>> Para solucionar los problemas que nos presenta tener el store en memoria, es necesario guardarlo en algun lugar.
 *   Para ello se usa redux-persist que permite guardar el store en el LocalStorage y asi poder acceder asi se cierre la pagina.
 * 
 * 	<<persistReducer>> Funcion que se usa para indicar cuales seran los redurces permanentes. Este recibe 2 elementos, 
 * 		1- La configuracion ya predefinida En la cual es necesario importar el storage de Redux-persist
 * 		2- Los reducer creados aparte con el combineReducers (Explicados en la parte de redux)
 * 	Esto crea un elemento reducer que se le pasa como parametros a el configureStore en elemento reducer
 * 
 * 	<<persistStore>> Funcion en la cual se indica cual sera el store del que se guardara los datos.
 *  El elemento que devuelve se exporta para ser usado como proveedor en la declaracion de las rutas
 * 
 */

const reducer = combineReducers({
	user: userSlice,
});

const persistConfig = {
	key: 'root',
	storage,
};

const persistedReducer = persistReducer(persistConfig, reducer);

export const store = configureStore({
	reducer: persistedReducer,
});

export const persistor = persistStore(store);
