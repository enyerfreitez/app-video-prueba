import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import Axios from 'axios';
import apiConfig from '../config/api';

/** Redux
 *
 * <<createSlice>> Esta funcion crea un slice (o pedaso) del store en el cual se ingresara la informacion
 *  de un modulo en cuestion. la idea es que este separado por cada elemento de la app. Ejemplo: si tenemos
 *  User, Videos, Autos... se usara un slice para cada uno el cual nos permitira guardar y modificar la informacion
 *  necesaria para la app de estos elementos.
 *
 * <<createSlice.name>> La funcion createSlice recibe un obj, el cual uno de los elementos es el name,
 *  como su clave lo indica este se usa para identificar este slice.
 *
 * <<createSlice.initialState>> Dentro de este elemento se guarda el estado inicial del slice y un status,
 *  el cual normalmente es un string vacio.
 *
 * <<createSlice.reducers>> Dentro de este elemento se guardan las funciones encargadas de modificaran el estado
 *  del slice, Ejemplo: Una funcion para identificar el email del usuario logueado. Y la funcion para eliminar
 *  el valor de este slice. Cada una de estas funciones deben ser explorta para poder ser usuadas por la app
 *
 * <<createAsyncThunk>> Funcion que permite realizar reducer, pero estos devuelven una promesa, especiales
 *  para realizar diferentes peticiones a api o algun funcionamiento que devuelva una promesa, este recibe 2 parametros
 *  el primero es el nombre que llevara el reducer (type), mientras que el segundo parametros es la funcion asincrona a
 *  ejecutar.
 *
 * <<createSlice.extraReducers>> Dentro de este elemento se guarda las acciones a realizar segun los diferentes estados
 *  que devuelva la promesa. Teniendo en cuenta que las promesas poseen 3 estados:
 *      <pending> Este estado hace referencia cuando se envia la promesa. Aqui se puede enviar la accion que se desea
 *          que se ejecute al momento de enviar la promesa.
 *      <fulfilled> Este estado hace referencia cuando la promesa a sido realizada con exito. Aqui se ejecuta la accion
 *          necesario. Por ejemplo: Cambiar el valor al slice.
 *      <rejected> Este estado hace referencia cuando la promesa a fallado. Aqui se coloca el codigo a ejecutar, cuando
 *          falle la ejecucion.
 */

/** Axios
 *  Libreria para realizar peticiones, ya sea ajax o rest se hace el llamado y se le indica el tipo de peticion.
 *  esta recibe 2 parametros, la url a donde se realizara la peticion. Y un obj con los datos a enviar.
 *  Al ser una peticion devuelve una promesa por lo cual hay que usar el await para poder tomar los datos
 *  retornados por la peticion
 */

export const singUp = createAsyncThunk(
	'user/singUp',
	async ({ credentials }) => {
		const response = await Axios.post(`${apiConfig.domain}/users`, {
			user: credentials,
		});
		return response.data.user;
	}
);

export const signIn = createAsyncThunk(
	'user/signIn',
	async ({ credentials }) => {
		const response = await Axios.post(`${apiConfig.domain}/users/signin`, {
			user: credentials,
		});
		return response.data.user;
	}
);

const userSlice = createSlice({
	name: 'user',
	initialState: {
		user: null,
		status: '',
	},
	reducers: {
		logOut: (state) => {
			state.user = null;
			state.status = '';
		},
	},
	extraReducers: {
		[singUp.pending]: (state) => {
			state.status = 'loading';
		},
		[singUp.fulfilled]: (state, action) => {
			state.user = action.payload;
			state.status = 'success';
		},
		[singUp.rejected]: (state) => {
			state.status = 'failed';
		},

		[signIn.pending]: (state) => {
			state.status = 'loading';
		},
		[signIn.fulfilled]: (state, action) => {
			state.user = action.payload;
			state.status = 'success';
		},
		[signIn.rejected]: (state) => {
			state.status = 'failed';
		},
	},
});

export const { logOut } = userSlice.actions;

export default userSlice.reducer; // Se exporta el redurcer del Slice para implemetarlo en el configureStore
