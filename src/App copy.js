// Archivo de pruebas. Solo para definir ideas.

import {
  BrowserRouter,
  Routes,
  Route,
  Link,
  Outlet,
  useParams,
} from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "./store";

/** Redux
 *
 * <<Provider>> Elemento encargado de poder preveer la informacion del store por el resto de la app. Este recibe una Prop
 *  llamada store, la cual como su nombre lo indica es el el store creado con anterioridad. Y como hijos se le pasan todos
 *  los elementos que necesiten acceder a la informacion del store.
 *
 */

/** Routers
 *
 * <<BrowserRouter>> Elemento padre de las rutas, este indica que todos sus hijos seran rutas funcionales y permite navegar entre ellas
 *  solo es posible tener un BrowserRouter por aplicacion
 *
 * <<Routes>> Elemento que agrupa a las rutas, se puede ver como un grupo de rutas a las cuales se les aplicara diferentes elementos entre ellas
 *  se puede tener varios grupos por app. Ejemplo: Para las vistas de los usuarios y las vistas del adminPage.
 *
 * <<Route>> Elemento que define una ruta o subgrupo de las mismas, se pueden tener segun la cantidad de rutas que sean necesarias. Ademas es
 *  posible definir las subRutas dentro de la misma.
 *
 * <<Link>> Elemento que realiza la misma funcion que un href en HTML. Este fue creado porque el Href actualiza la pagina por completo. Mientras
 *  que este solo actualiza la parte necesaria de la app. Ejemplo: En una pagina donde se maneja un footer o una narvar solo es necesario actualizar
 *  el contenido de la pagina no el footer ni el narvar, al menos que sea necesario.
 *
 * <<Outlet>> Elemento que permite indicarle a la web que se actualizara la informacion donde este se ubique ayudando a el ejemplo de arriba.
 *
 * <<useParams>> Hook usado para poder acceder a los parametros enviados en la URL, por el ejemplo el id de un elemento en la DB. Dicho hook
 *  devuelve un obj el cual se puede desectructurar para obtener el parametro necesario.
 *
 */

const NotImplemented = () => {
  return (
    <>
      <Link to="/videos">Ir a videos</Link>
      <p>Esta pagina aun no esta lista</p>
    </>
  );
};

const Error404 = () => {
  return (
    <>
      <Link to="/">Ir al Inicio</Link>
      <p>Esta pagina no existe</p>
    </>
  );
};

const UsersOutlet = () => {
  return (
    <>
      <Outlet />
    </>
  );
};

const VideoByID = () => {
  const { id } = useParams();
  return (
    <>
      <p>{id}</p>
    </>
  );
};

function App() {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <Routes>
          <Route path="/" element={<NotImplemented />} />

          <Route path="/usuarios" element={<UsersOutlet />}>
            <Route path="registro" element={<NotImplemented />} />
            <Route path="login" element={<NotImplemented />} />
            <Route path=":id" element={<NotImplemented />} />
            <Route path=":id/videos" element={<NotImplemented />} />
          </Route>

          <Route path="/videos">
            <Route path="" element={<NotImplemented />} />
            <Route path="nuevo" element={<NotImplemented />} />
            <Route path=":id" element={<VideoByID />} />
          </Route>

          <Route path="*" element={<Error404 />} />
        </Routes>
      </Provider>
    </BrowserRouter>
  );
}

export default App;
