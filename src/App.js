// Archivo de pruebas. Solo para definir ideas.

import {
	BrowserRouter,
	Routes,
	Route,
	Link,
	Outlet,
	useParams,
	useNavigate,
} from 'react-router-dom';
import { Provider, useSelector, useDispatch } from 'react-redux';

import { store, persistor } from './store';
import SignIn from './users/SignIn';
import { logOut } from './store/user';
import { PersistGate } from 'redux-persist/integration/react';

/** Redux
 *
 * <<Provider>> Elemento encargado de poder proveer la informacion del store por el resto de la app. Este recibe un Prop
 *  llamado store, el cual como su nombre lo indica es el store creado con anterioridad. Y como hijos, se le pasan todos
 *  los elementos que necesiten acceder a la informacion del store.
 */

/** Routers
 *
 * <<BrowserRouter>> Elemento padre de las rutas, este indica que todos sus hijos seran rutas funcionales y permite navegar entre ellas
 *  solo es posible tener un BrowserRouter por aplicacion
 *
 * <<Routes>> Elemento que agrupa a las rutas, se puede ver como un grupo de rutas a las cuales se les aplicara diferentes elementos entre ellas
 *  se puede tener varios grupos por app. Ejemplo: Para las vistas de los usuarios y las vistas del adminPage.
 *
 * <<Route>> Elemento que define una ruta o subgrupo de las mismas, se pueden tener segun la cantidad de rutas que sean necesarias. Ademas es
 *  posible definir las subRutas dentro de la misma.
 *
 * <<Link>> Elemento que realiza la misma funcion que un href en HTML. Este fue creado porque el Href actualiza la pagina por completo. Mientras
 *  que este solo actualiza la parte necesaria de la app. Ejemplo: En una pagina donde se maneja un footer o una narvar solo es necesario actualizar
 *  el contenido de la pagina no el footer ni el narvar, al menos que sea necesario.
 *
 * <<Outlet>> Elemento que permite indicarle a la web que se actualizara la informacion donde este se ubique ayudando a el ejemplo de arriba.
 *
 * <<useParams>> Hook usado para poder acceder a los parametros enviados en la URL, por el ejemplo el id de un elemento en la DB. Dicho hook
 *  devuelve un obj el cual se puede desectructurar para obtener el parametro necesario.
 *
 * <<useNavigate>> Hook usado para redureccionar la pagina a otra sin necesidad de cargar la app. Solo se actualiza segun la pagina el DOM
 *
 */

/** redux-persist
 *
 * 	<<PersistGate>> Es un elemento que se encarga de proveer el store persistente a los elementos Router creados en su interior.
 * 	 Recibe 2 props
 * 		1- <loading> Viene siendo el elemento que se muestra mientras que se accede a la informacion del LocalStorage y se pasa al store
 * 		2- <persistor> Es el elemento que se exporta en las creacion del store, este contiene la instancia del store persistente
 */

const NotImplemented = () => {
	return (
		<>
			<Link to='/videos'>Ir a videos</Link>
			<p>Esta pagina aun no esta lista</p>
		</>
	);
};

const Error404 = () => {
	return (
		<>
			<Link to='/'>Ir al Inicio</Link>
			<p>Esta pagina no existe</p>
		</>
	);
};

const UsersOutlet = () => {
	const dispatch = useDispatch();
	const user = useSelector((state) => state.user.user);
	const navigate = useNavigate();

	const doLogOut = () => {
		dispatch(logOut());
		navigate('/');
	};

	return (
		<>
			{user && <button onClick={doLogOut}>Cerrar sesion</button>}
			<Outlet />
		</>
	);
};

const VideoByID = () => {
	const { id } = useParams();
	return (
		<>
			<p>{id}</p>
		</>
	);
};

function App() {
	return (
		<BrowserRouter>
			<Provider store={store}>
				<PersistGate loading={null} persistor={persistor}>
					<Routes>
						<Route path='/' element={<NotImplemented />} />

						<Route path='/usuarios' element={<UsersOutlet />}>
							<Route path='registro' element={<NotImplemented />} />
							<Route path='login' element={<SignIn />} />
							<Route path=':id' element={<NotImplemented />} />
							<Route path=':id/videos' element={<NotImplemented />} />
						</Route>

						<Route path='/videos'>
							<Route path='' element={<NotImplemented />} />
							<Route path='nuevo' element={<NotImplemented />} />
							<Route path=':id' element={<VideoByID />} />
						</Route>

						<Route path='*' element={<Error404 />} />
					</Routes>
				</PersistGate>
			</Provider>
		</BrowserRouter>
	);
}

export default App;
