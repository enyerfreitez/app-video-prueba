import { useDispatch, useSelector } from 'react-redux';
import { logOut, singUp } from '../store/user';

/** Redux
 *
 * <<useDispatch>> Hook que permite acceder a los reducers realizados en el createSlice del store
 *  este hook tiene una funcion en la cual se puede llamar a la funcion necesaria para modificar los
 *  valores del store.
 *
 * <<useSelector>> Hook que permite acceder a la informacion del store. Para leer los datos se recibe
 *  una funcion donde por parametros lleva el state actual. Y por medio de el se puede acceder
 *  al Slice creado en el store.
 *
 */

const SignUp = () => {
	const dispatch = useDispatch();

	const user = useSelector((state) => state.user.user);

	const doSignIn = () => {
		dispatch(
			/* signIn({
				email: 'enyerfreitez@gmail.com',
				jwt: 'asdasjda7sda76s7nasnk',
			}) */
			singUp({
				credentials: {
					email: 'magapebric04@gmail.com',
					password: '12345678',
					username: 'enyer95',
				},
			})
		);
	};

	const doLogOut = () => {
		dispatch(logOut());
	};

	return (
		<div>
			{user ? (
				<button onClick={doLogOut}>Salir</button>
			) : (
				<button onClick={doSignIn}>Ingresar</button>
			)}
		</div>
	);
};

export default SignUp;
